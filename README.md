# README #

This is a simple demo that set up an environment where two or more clients can share video and their screens.

### Running the app ###

First, clone this repository and be sure you have node (v0.10.37) installed in your pc.

Download the dependencies using npm in this directory

```
$ npm install
```

Create your own certificate:

```
$ openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem
```


Add the line below to your /etc/hosts file:

```
127.0.0.1    <yourname>.tokbox.com
```

To start the server run the following command:

```
$ node index.js
```

Now, you have the server running at https://<yourname>.tokbox.com:8080