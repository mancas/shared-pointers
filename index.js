var OpenTok = require('opentok');
var express = require('express');
var https = require('https');
var fs = require('fs');
var options = {
   key  : fs.readFileSync('key.pem'),
   cert : fs.readFileSync('cert.pem')
};

// Verify that the API Key and API Secret are defined
var apiKey = '45418552',
    apiSecret = '6375372f89daa1a04c167a86c9bc3b846c2ea27d';
if (!apiKey || !apiSecret) {
  console.log('You must specify API_KEY and API_SECRET');
  process.exit(1);
}

// Initialize the express app
var app = express();
app.use(express.static(__dirname + '/public'));

// Initialize OpenTok
var opentok = new OpenTok(apiKey, apiSecret);

// Create a session and store it in the express app
opentok.createSession(function(err, session) {
  if (err) throw err;
  app.set('sessionId', session.sessionId);
  // We will wait on starting the app until this is done
  init();
});

app.get('/', function(req, res) {
  var sessionId = app.get('sessionId'),
      // generate a fresh token for this client
      token = opentok.generateToken(sessionId);

  res.render('index.ejs', {
    apiKey: apiKey,
    sessionId: sessionId,
    token: token
  });
});

// Start the express app
function init() {
  https.createServer(options, app).listen(8080, function() {
    console.log('You\'re app is now ready at https://<yourname>.tokbox.com:8080');
  });
}