'use strict';

(function(exports){
    // 'screen', 'window', 'tab', 'browser', 'application'
    const SOURCE_TYPE = 'window';

    var App = {
        // Initialize an OpenTok Session object
        session: undefined,
        screenshare: undefined,
        screenshareElement: undefined,
        screenshareBtn: undefined,
        cursor: undefined,
        mouseHandler: undefined,

        lastTime: 0,
        MIN_TIME: 100,
        lastPosX: -1,
        lastPosY: -1,

        init: function() {
            console.log("init");
            var self = this;
            this.mouseHandler = this.mouseMoveHandler.bind(this);
            this.clickHandler = this.mouseClickHandler.bind(this);
            this.screenshareBtn = document.querySelector('#share-screen');
            this.session = TB.initSession(sessionId);
            this.session.on({
              // This function runs when another client publishes a stream (eg. session.publish())
              streamCreated: function(event) {
                // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
                // the element with id="subscribers"
                self.createScreenShareElement();
                document.body.removeChild(self.screenshareBtn);

                if (event.stream.videoType === 'screen') {
                    self.addMouseListener();
                    self.addClickListener();
                    self.cursor.dataset.color = 2;
                }

                var options = {
                    maxResolution: { width: 1280, height: 720 },
                    height: '100%',
                    width: '100%',
                    insertMode: 'append'
                };

                // Subscribe to the stream that caused this event, put it inside the container we just made
                self.session.subscribe(event.stream, self.screenshareElement, options);
              },

              streamDestroyed: function(event) {
                self.removeMouseListener();
                self.removeClickListener();
              },

              connectionCreated: function(event) {
                console.info(event, self.session.connection.id);
                if (!self.isMyself(event.connection.id)) {
                    self.createCursorOverlay(event.connection.id);
                }
              },
              "signal:mousemove": function(signal) {
                console.info(signal);
                if (self.isMyself(signal.data.connectionId)) {
                    console.info('It is my own mouse so let"s discard the event');
                    return;
                }
                self.cursor.style.top = signal.data.top + 'px';
                self.cursor.style.left = signal.data.left + 'px';
              },
              "signal:click": function(signal) {
                console.info(signal);
                if (self.isMyself(signal.data.connectionId)) {
                    console.info('It is my own mouse so let"s discard the event');
                    return;
                }
                console.log('CHANGING CLASS');
                self.cursor.classList.add('clicked');
                setTimeout(function() {
                    console.log('BACK TO NORMAL');
                    self.cursor.classList.remove('clicked');
                }, 1000);

              }
            });

            // Connect to the Session using the 'apiKey' of the application and a 'token' for permission
            this.session.connect(apiKey, token, function(error) {
                if (error) {
                    console.error("Error connecting to session");
                    return;
                }

                // Screen sharing is available. Publish the screen.
                var options = {
                    videoSource: SOURCE_TYPE,
                    maxResolution: { width: 1280, height: 720 },
                    height: '100%',
                    width: '100%',
                    insertMode: 'append'
                };

                self.screenshareBtn.addEventListener('click', function() {
                    self.createScreenShareElement();
                    self.addMouseListener();
                    self.addClickListener();

                    self.screenshare = TB.initPublisher(self.screenshareElement, options, function(error) {
                        if (error) {
                            console.log('Error initializing screenshare. ' + error.message);
                            return;
                        }

                        // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
                        // clients)
                        self.session.publish(self.screenshare, function(error) {
                            if (error) {
                                console.error('Error while publishing');
                                return;
                            }

                            document.body.removeChild(self.screenshareBtn);
                        });
                    });
                });
            });
        },

        createScreenShareElement: function() {
            if (typeof this.screenshareElement !== 'undefined') {
                return;
            }

            this.screenshareElement = document.createElement('div');
            this.screenshareElement.id = 'screen-preview';
            document.body.appendChild(this.screenshareElement);
        },

        createCursorOverlay: function(id) {
            if (typeof this.cursor !== 'undefined') {
                return;
            }

            this.cursor = document.createElement('div');
            this.cursor.id = 'cursor-' + id;
            document.body.appendChild(this.cursor);
        },

        addClickListener: function() {
            this.screenshareElement.addEventListener('click', this.clickHandler);
        },

        removeClickListener: function() {
            this.screenshareElement.removeEventListener('click', this.clickHandler);
        },

        mouseClickHandler: function(event) {
            var now = Date.now();
            if (now - this.lastTime < this.MIN_TIME) {
                return;
            }
            var pageX = event.pageX;
            var pageY = event.pageY;
            console.log("dispatching click at " + pageX + " - " + pageY);

            this.session.signal({
                type: "click",
                data: {
                    top: pageY,
                    left: pageX,
                    connectionId: this.session.connection.connectionId
                }
            }, function finished(error) {
                if (error) {
                    console.error("Error sending signal");
                    return;
                }

                console.info("Signal dispatched");
            });
        },

        addMouseListener: function() {
            this.screenshareElement.addEventListener('mousemove', this.mouseHandler);
        },

        removeMouseListener: function() {
            this.screenshareElement.removeEventListener('mousemove', this.mouseHandler);
        },

        mouseMoveHandler: function(event) {
            var now = Date.now();
            if (now - this.lastTime < this.MIN_TIME) {
                return;
            }

            this.lastTime = now;
            var pageX = event.pageX;
            var pageY = event.pageY;

            if (Math.abs(this.lastPosX - pageX) < 3 && Math.abs(this.lastPosY - pageY) < 3) {
                console.error("hasn't moved enough; returning");
                // Not a substantial enough change
                return;
            }

            this.lastPosX = pageX;
            this.lastPosY = pageY;
            var target = event.target
            console.log("dispatching, top", pageY, "left", pageX);
            this.session.signal({
                type: "mousemove",
                data: {
                    top: pageY,
                    left: pageX,
                    connectionId: this.session.connection.connectionId
                }
            }, function(error) {
                if (error) {
                    console.error("Error sending signal");
                    return;
                }

                console.info("Signal dispatched");
            });
            return;
        },

        isMyself: function(connectionId) {
          return this.session &&
                 this.session.connection.connectionId === connectionId;
        }
    };

    exports.App = App;

})(window);

window.addEventListener('DOMContentLoaded', function() {
    console.info('here');
    window.App.init();
});
