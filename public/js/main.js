'use strict';

(function(exports){
    var App = {
        // Initialize an OpenTok Session object
        session: undefined,

        // Initialize a Publisher, and place it into the element with id="publisher"
        publisher: undefined,

        mouseHandler: undefined,

        lastTime: 0,
        MIN_TIME: 100,
        lastPosX: -1,
        lastPosY: -1,

        init: function() {
            console.log("init");
            var self = this;
            this.mouseHandler = this.mouseMoveHandler.bind(this);
            this.session = TB.initSession(sessionId);
            this.session.on({
              // This function runs when another client publishes a stream (eg. session.publish())
              streamCreated: function(event) {
                // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
                // the element with id="subscribers"
                var subContainer = document.createElement('div');
                subContainer.id = 'stream-' + event.stream.streamId;
                var id = event.stream.videoType === 'screen' ? 'screenshare' : 'subscribers';
                document.getElementById(id).appendChild(subContainer);
console.info(event);
                if (event.stream.videoType === 'screen') {
                    self.addMouseListener();
                }

                // Subscribe to the stream that caused this event, put it inside the container we just made
                self.session.subscribe(event.stream, subContainer);
              },

              streamDestroyed: function(event) {
                self.removeMouseListener();
              }
            });

            var div = document.createElement('div');
                div.style.color = 'red';
                div.textContent = '\\';
                div.style.position = 'absolute';
                div.style.zIndex = '9999';

            document.body.appendChild(div);

            // Connect to the Session using the 'apiKey' of the application and a 'token' for permission
            this.session.connect(apiKey, token, function(error) {
                if (error) {
                    console.error("Error connecting to session");
                    return;
                }

                self.publisher = TB.initPublisher(apiKey, 'publisher', function(error) {
                    if (error) {
                        console.log('Error initializing publisher. ' + error.message);
                        return;
                    }

                    // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
                    // clients)
                    self.session.publish(self.publisher);
                });
            });

            var btn = document.querySelector('#share-screen');
            btn.addEventListener('click', function() {
                self.screenshare();
            });
        },

        screenshare: function() {
            var self = this;
            TB.checkScreenSharingCapability(function(response) {
                if (!response.supported || response.extensionRegistered === false) {
                  alert('This browser does not support screen sharing.');
                } else if (response.extensionInstalled === false) {
                  alert('Please install the screen sharing extension and load your app over https.');
                } else {
                    // Screen sharing is available. Publish the screen.
                    var options = {
                        videoSource: 'screen',
                        maxResolution: { width: 1280, height: 720 }
                    };

                    self.session.on({
                        accessAllowed: function() {
                            console.info('accessAllowed');
                            self.session.on("signal:mousemove", function(signal) {
                                console.info(signal.data);
                                div.style.top = signal.data.top + 'px';
                                div.style.left = signal.data.left + 'px';
                            });

                            self.session.publish(screenSharingPublisher, function(error) {
                                if (error) {
                                  alert('Could not share the screen: ' + error.message);
                                }
                            });
                        }
                    });

                    var screenSharingPublisher =
                        TB.initPublisher('screen-preview', options);
                }
            });
        },

        addMouseListener: function() {
            var element = document.getElementById('screenshare');
            element.addEventListener('mousemove', this.mouseHandler);
        },

        removeMouseListener: function() {
            var element = document.getElementById('screenshare');
            element.removeEventListener('mousemove', this.mouseHandler);
        },

        mouseMoveHandler: function(event) {
            var now = Date.now();
            if (now - this.lastTime < this.MIN_TIME) {
                return;
            }

            this.lastTime = now;
            var pageX = event.pageX;
            var pageY = event.pageY;

            if (Math.abs(this.lastPosX - pageX) < 3 && Math.abs(this.lastPosY - pageY) < 3) {
                console.error("hasn't moved enough; returning");
                // Not a substantial enough change
                return;
            }

            this.lastPosX = pageX;
            this.lastPosY = pageY;
            var target = event.target 
            console.log("dispatching, top", pageY, "left", pageX);
            this.session.signal({
                type: "mousemove",
                data: {
                    top: pageY,
                    left: pageX
                }
            }, function(error) {
                if (error) {
                    console.error("Error sending signal");
                    return;
                }

                console.info("Signal dispatched");
            });
            return;
        }
    };

    exports.App = App;

})(window);

window.addEventListener('DOMContentLoaded', function() {
    console.info('here');
    window.App.init();
});